package com.verify.bovquier.websourceviewer.addressreader.contract;

import com.verify.bovquier.websourceviewer.R;
import com.verify.bovquier.websourceviewer.base.BaseRequestObserver;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author bovquier
 *         on 17.09.2017.
 */

@RunWith(RobolectricTestRunner.class)
public class AddressReaderPresenterTest {
    @Mock
    private AddressReaderContract.View mView;
    @Mock
    private AddressReaderContract.Model mModel;
    @InjectMocks
    private AddressReaderPresenter addressReaderPresenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        addressReaderPresenter.setView(mView);
    }

    @Test
    public void testSubmitEmptyWebAddress() throws Exception {
        when(mView.getWebAddress()).thenReturn(" ");
        addressReaderPresenter.submitWebAddress();
        verify(mView).getWebAddress();
        verify(mView).showInputError(R.string.error_empty_address);
        verify(mView).hideKeyboard();
    }

    @Test
    public void testSubmitNotEmptyWebAddressNotMatchingPattern() throws Exception {
        when(mView.getWebAddress()).thenReturn("test....me");
        addressReaderPresenter.submitWebAddress();
        verify(mView).getWebAddress();
        verify(mView).showErrorBar("Address does not match Web Url Pattern", true, R.string.clear);
        verify(mView).hideKeyboard();
    }

    @Test
    public void testSubmitNotEmptyMatchedWebAddressWhenOnline() throws Exception {
        String url = "test.me";
        when(mView.getWebAddress()).thenReturn(url);
        when(mView.isOnline()).thenReturn(true);
        addressReaderPresenter.submitWebAddress();
        verify(mView).getWebAddress();
        verify(mView).isOnline();
        verify(mView).showProgress(true);
        verify(mModel).loadWebContent(url);
        verify(mView).hideKeyboard();
    }

    @Test
    public void testSubmitNotEmptyMatchedWebAddressWhenNotOnline() throws Exception {
        String value = "test.me";
        when(mView.getWebAddress()).thenReturn(value);
        when(mView.isOnline()).thenReturn(false);
        addressReaderPresenter.submitWebAddress();
        verify(mView).getWebAddress();
        verify(mView).isOnline();
        verify(mView).showErrorBar("No internet connection", false, -1);
        verify(mView).hideKeyboard();
    }

    @Test
    public void testCreateObserver() throws Exception {
        BaseRequestObserver result = addressReaderPresenter.createObserver();
        assertTrue(result instanceof AddressReaderPresenter.AddressBodyObserver);
    }

    @Test
    public void testReload() throws Exception {
        addressReaderPresenter.setMWebAddress("asd");
        addressReaderPresenter.reload();
        verify(mView).showProgress(true);
        verify(mModel).loadWebContent("asd");
    }

    @Test
    public void testClearAddress() throws Exception {
        addressReaderPresenter.clearAddress();
        assertNull(addressReaderPresenter.getMWebAddress());
        verify(mView).clearAddress();
    }
}