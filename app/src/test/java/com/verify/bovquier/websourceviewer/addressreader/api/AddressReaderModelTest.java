package com.verify.bovquier.websourceviewer.addressreader.api;

import com.verify.bovquier.websourceviewer.addressreader.api.datamodel.WebContent;
import com.verify.bovquier.websourceviewer.addressreader.contract.AddressReaderContract;
import com.verify.bovquier.websourceviewer.addressreader.contract.AddressReaderPresenter;
import com.verify.bovquier.websourceviewer.addressreader.repository.ContentRepository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author bovquier
 *         on 21.09.2017.
 */
public class AddressReaderModelTest {
    @Mock
    private AddressReaderContract.Service mService;
    @Mock
    private ContentRepository mContentRepo;
    @Mock
    private AddressReaderPresenter.AddressBodyObserver mObserver;
    @InjectMocks
    private AddressReaderModel addressReaderModel;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testLoadWebContentWithoutStoredData() throws Exception {
        String webAddress = "webAddress";
        when(mContentRepo.hasContent(webAddress)).thenReturn(false);
        addressReaderModel.loadWebContent(webAddress);
        verify(mService).setWebAddress(webAddress);
        verify(mService).load();
    }

    @Test
    public void testLoadWebContentWithStoredData() throws Exception {
        String webAddress = "webAddress";
        when(mContentRepo.hasContent(webAddress)).thenReturn(true);
        when(mService.getObserver()).thenReturn(mObserver);
        addressReaderModel.loadWebContent(webAddress);
        verify(mContentRepo).getContent(webAddress);
        verify(mService, never()).setWebAddress(webAddress);
        verify(mService, never()).load();
    }

    @Test
    public void testSetWebContent() throws Exception {
        String webAddress = "webAddress";
        WebContent content = new WebContent("content");
        addressReaderModel.setWebContent(webAddress, content);
        verify(mContentRepo).saveWebSite(webAddress, content);
    }
}