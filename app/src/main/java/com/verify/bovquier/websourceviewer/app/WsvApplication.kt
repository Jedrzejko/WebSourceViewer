package com.verify.bovquier.websourceviewer.app

import android.app.Application

import com.orhanobut.hawk.Hawk

/**
 * @author bovquier
 * on 16.09.2017.
 */

class WsvApplication : Application() {

    val component: ApplicationComponent by lazy {
        DaggerApplicationComponent.builder().build()
    }

    override fun onCreate() {
        super.onCreate()
        component.inject(this)
        Hawk.init(this).build()
        Hawk.deleteAll()
    }
}
