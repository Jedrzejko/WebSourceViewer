package com.verify.bovquier.websourceviewer.base

/**
 * @author bovquier
 * on 20.09.2017.
 */

abstract class BaseService<DM : BaseDataModel> : BaseContract.Service<DM> {

    override var observer: BaseRequestObserver<DM>? = null
}
