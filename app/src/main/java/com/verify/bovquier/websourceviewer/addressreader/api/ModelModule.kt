package com.verify.bovquier.websourceviewer.addressreader.api

import com.verify.bovquier.websourceviewer.addressreader.api.datamodel.WebContent
import com.verify.bovquier.websourceviewer.addressreader.contract.AddressReaderContract
import com.verify.bovquier.websourceviewer.addressreader.repository.ContentRepository
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import org.jsoup.Jsoup
import retrofit2.Converter
import retrofit2.Retrofit
import java.io.IOException
import java.lang.reflect.Type

/**
 * @author bovquier
 * on 16.09.2017.
 */

@Module
class ModelModule {
    /**
     * based on:
     * https://stackoverflow.com/questions/44280410/get-html-of-a-website-with-retrofit-android
     *
     * @return [WebApi] for downloading htmls
     */
    @Provides
    fun provideApi(): WebApi {
        return provideRetrofit(provideClient()).create(WebApi::class.java)
    }

    private fun provideRetrofit(client: OkHttpClient): Retrofit {
        return Retrofit.Builder().baseUrl("http:\\www.w.w").client(client)
                .addConverterFactory(WebContentAdapter.FACTORY).build()
    }

    private fun provideClient(): OkHttpClient {
        return OkHttpClient.Builder().followRedirects(true).build()
    }

    @Provides
    fun provideContentRepository(): ContentRepository {
        return ContentRepository()
    }

    @Provides
    fun provideService(api: WebApi): AddressReaderContract.Service {
        return AddressReaderService(api)
    }

    class WebContentAdapter : Converter<ResponseBody, WebContent> {

        @Throws(IOException::class)
        override fun convert(responseBody: ResponseBody): WebContent {
            val document = Jsoup.parse(responseBody.string())
            val value = document.select("html")[0]
            val content = value.html()
            return WebContent(content)
        }

        companion object {
            val FACTORY: Converter.Factory = object : Converter.Factory() {
                override fun responseBodyConverter(type: Type?,
                                                   annotations: Array<Annotation>?,
                                                   retrofit: Retrofit?): Converter<ResponseBody, *>? {
                    return if (type === WebContent::class.java) WebContentAdapter() else null
                }
            }
        }
    }
}
