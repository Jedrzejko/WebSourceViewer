package com.verify.bovquier.websourceviewer.base

import okhttp3.Response

/**
 * @author bovquier
 * on 16.09.2017.
 */

abstract class BaseRequestObserver<in DM : BaseDataModel> {
    abstract fun onResponseBodyReceived(response: DM)

    abstract fun onRequestFailure(t: Throwable)

    abstract fun onResponseNotOk(code: Int, raw: Response)
}
