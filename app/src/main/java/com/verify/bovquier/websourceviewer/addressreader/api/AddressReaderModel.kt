package com.verify.bovquier.websourceviewer.addressreader.api

import com.verify.bovquier.websourceviewer.addressreader.api.datamodel.WebContent
import com.verify.bovquier.websourceviewer.addressreader.contract.AddressReaderContract
import com.verify.bovquier.websourceviewer.addressreader.repository.ContentRepository
import com.verify.bovquier.websourceviewer.base.BaseModel
import com.verify.bovquier.websourceviewer.base.BaseRequestObserver

/**
 * @author bovquier
 * on 16.09.2017.
 */

class AddressReaderModel(private val mService: AddressReaderContract.Service, private val mContentRepo: ContentRepository) : BaseModel<WebContent>(), AddressReaderContract.Model {

    private val observer: BaseRequestObserver<WebContent>?
        get() = mService.observer

    override fun loadWebContent(webAddress: String) {
        if (mContentRepo.hasContent(webAddress)) {
            observer?.onResponseBodyReceived(mContentRepo.getContent(webAddress))
            return
        }
        mService.setWebAddress(webAddress)
        mService.load()
    }

    override fun setWebContent(webAddress: String, body: WebContent) {
        mContentRepo.saveWebSite(webAddress, body)
    }

    override fun setRequestObserver(observer: BaseRequestObserver<WebContent>) {
        mService.observer = observer
    }
}
