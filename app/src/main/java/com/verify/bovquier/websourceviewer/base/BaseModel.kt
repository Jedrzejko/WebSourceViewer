package com.verify.bovquier.websourceviewer.base

/**
 * @author bovquier
 * on 16.09.2017.
 */

abstract class BaseModel<DM : BaseDataModel> : BaseContract.Model<DM>
