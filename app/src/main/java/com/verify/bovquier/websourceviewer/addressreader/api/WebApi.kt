package com.verify.bovquier.websourceviewer.addressreader.api

import com.verify.bovquier.websourceviewer.addressreader.api.datamodel.WebContent

import okhttp3.HttpUrl
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url

/**
 * @author bovquier
 * on 16.09.2017.
 */

interface WebApi {
    @GET
    fun get(@Url url: HttpUrl): Call<WebContent>
}
