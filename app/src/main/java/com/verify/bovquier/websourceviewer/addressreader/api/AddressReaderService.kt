package com.verify.bovquier.websourceviewer.addressreader.api

import com.orhanobut.logger.Logger
import com.verify.bovquier.websourceviewer.addressreader.api.datamodel.WebContent
import com.verify.bovquier.websourceviewer.addressreader.contract.AddressReaderContract
import com.verify.bovquier.websourceviewer.base.BaseService
import okhttp3.HttpUrl
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * @author bovquier
 * on 20.09.2017.
 */

class AddressReaderService(private val mApi: WebApi) : BaseService<WebContent>(), AddressReaderContract.Service {
    private var url: HttpUrl? = null

    override fun load() {
        if (url == null) {
            observer?.onRequestFailure(AddressLostException())
            return
        }
        performCall(mApi.get(url!!))
    }

    private fun performCall(call: Call<WebContent>) {
        call.enqueue(object : Callback<WebContent> {
            override fun onResponse(call: Call<WebContent>, response: Response<WebContent>) {
                Logger.d("hasResponse: " + response.code())
                if (response.code() == 200) {
                    response.body()?.let { observer?.onResponseBodyReceived(it) }
                } else {
                    observer?.onResponseNotOk(response.code(), response.raw())
                }
            }

            override fun onFailure(call: Call<WebContent>, t: Throwable) {
                observer?.onRequestFailure(t)
            }
        })
    }

    override fun setWebAddress(webAddress: String) {
        if (webAddress.isBlank()) {
            url = null
            return
        }
        url = if (!webAddress.startsWith("http")) {
            HttpUrl.parse("http://" + webAddress)
        } else {
            HttpUrl.parse(webAddress)
        }
    }

    inner class AddressLostException internal constructor() : Throwable("Address lost - provide " +
            "new one")
}
