package com.verify.bovquier.websourceviewer.base

import com.verify.bovquier.websourceviewer.base.BaseContract.View

/**
 * @author bovquier
 * on 16.09.2017.
 */

abstract class BasePresenter<DM : BaseDataModel, V : View, M : BaseContract.Model<DM>> :
        BaseContract.Presenter<V, M> {
    override var view: V? = null
    override var model: M? = null
        set(value) {
            field = value
            model!!.setRequestObserver(createObserver())
        }

    protected abstract fun createObserver(): BaseRequestObserver<DM>
}
