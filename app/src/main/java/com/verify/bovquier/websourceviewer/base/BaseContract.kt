package com.verify.bovquier.websourceviewer.base

/**
 * @author bovquier
 * on 15.09.2017.
 */

interface BaseContract {
    interface Presenter<V : View, M : Model<*>> {
        var view: V?

        var model: M?

        fun reload()

        fun clearAddress()
    }

    interface View {

        val isOnline: Boolean
        fun hideKeyboard()
    }

    interface Model<out DM : BaseDataModel> {
        fun setRequestObserver(observer: BaseRequestObserver<DM>)
    }

    interface Service<DM : BaseDataModel> {

        var observer: BaseRequestObserver<DM>?
        fun load()
    }
}
