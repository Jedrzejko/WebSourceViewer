package com.verify.bovquier.websourceviewer.app

import com.verify.bovquier.websourceviewer.addressreader.AddressReaderActivity
import com.verify.bovquier.websourceviewer.addressreader.AddressReaderModule
import com.verify.bovquier.websourceviewer.addressreader.api.ModelModule
import dagger.Component
import javax.inject.Singleton

/**
 * @author bovquier
 * on 16.09.2017.
 */

@Singleton
@Component(modules = arrayOf(ApplicationModule::class, AddressReaderModule::class, ModelModule::class))
interface ApplicationComponent {
    fun inject(application: WsvApplication)
    fun inject(application: AddressReaderActivity)
}
