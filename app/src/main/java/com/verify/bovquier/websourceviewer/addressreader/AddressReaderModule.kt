package com.verify.bovquier.websourceviewer.addressreader

import com.verify.bovquier.websourceviewer.addressreader.api.AddressReaderModel
import com.verify.bovquier.websourceviewer.addressreader.contract.AddressReaderContract
import com.verify.bovquier.websourceviewer.addressreader.contract.AddressReaderPresenter
import com.verify.bovquier.websourceviewer.addressreader.repository.ContentRepository

import dagger.Module
import dagger.Provides

/**
 * @author bovquier
 * on 16.09.2017.
 */

@Module
class AddressReaderModule {

    @Provides
    fun provideAddressProviderPresenter(model: AddressReaderContract.Model):
            AddressReaderContract.Presenter<AddressReaderContract.View, AddressReaderContract.Model> {
        return AddressReaderPresenter(model)
    }

    @Provides
    fun provideAddressReaderModel(service: AddressReaderContract.Service,
                                  cr: ContentRepository): AddressReaderContract.Model {
        return AddressReaderModel(service, cr)
    }
}
