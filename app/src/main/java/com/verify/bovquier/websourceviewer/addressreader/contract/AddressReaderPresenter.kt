package com.verify.bovquier.websourceviewer.addressreader.contract

import android.annotation.SuppressLint
import androidx.annotation.VisibleForTesting
import android.util.Patterns
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import com.verify.bovquier.websourceviewer.R
import com.verify.bovquier.websourceviewer.addressreader.api.datamodel.WebContent
import com.verify.bovquier.websourceviewer.addressreader.contract.AddressReaderContract.*
import com.verify.bovquier.websourceviewer.base.BasePresenter
import com.verify.bovquier.websourceviewer.base.BaseRequestObserver
import okhttp3.Response

/**
 * @author bovquier
 * on 16.09.2017.
 */

class AddressReaderPresenter(model: Model) : BasePresenter<WebContent, View, Model>(),
        Presenter<View, Model> {

    var mWebAddress: String? = null

    init {
        this.model = model
        Logger.addLogAdapter(AndroidLogAdapter())
    }

    @SuppressLint("VisibleForTests")
    override fun submitWebAddress() {
        view?.let {
            val webAddress = it.getWebAddress().trim { it <= ' ' }
            if (webAddress.isBlank()) {
                it.showInputError(R.string.error_empty_address)
            } else if (matchesWebUrlPattern(webAddress)) {
                if (it.isOnline) {
                    mWebAddress = webAddress
                    it.showProgress(true)
                    model?.loadWebContent(webAddress)
                } else {
                    it.showErrorBar("No internet connection", false, -1)
                }
            } else {
                it.showErrorBar("Address does not match Web Url Pattern", true, R.string.clear)
            }
            it.hideKeyboard()
        }
    }

    @VisibleForTesting
    private fun matchesWebUrlPattern(webAddress: String): Boolean {
        return Patterns.WEB_URL.matcher(webAddress).matches()
    }

    @VisibleForTesting
    public override fun createObserver(): BaseRequestObserver<WebContent> {
        return AddressBodyObserver()
    }

    private fun manageError(code: Int, raw: okhttp3.Response) {
        Logger.w("Error while loading page content: " + code + "\n" + raw.message())
        val message = raw.message()
        view?.let {
            when (code) {
                404 -> it.showInputError(R.string.error_page_not_found)
                500 -> it.showRetryBar(R.string.error_server_error)
                else //and many others
                -> it.showErrorBar(code.toString() + message, true, R.string.clear)
            }
            it.showErrorBar(raw.message() + ": " + code, true, R.string.clear)
        }
    }

    override fun reload() {
        view?.showProgress(true)
        model?.loadWebContent(mWebAddress!!)
    }

    override fun clearAddress() {
        mWebAddress = null
        view?.clearAddress()
    }

    inner class AddressBodyObserver : BaseRequestObserver<WebContent>() {
        override fun onResponseBodyReceived(response: WebContent) {
            view?.showProgress(false)
            if (response.content.isNotBlank()) {
                val content = response.content
                view?.showContent(content)
                model?.setWebContent(mWebAddress!!, response)
                mWebAddress = null
            } else {
                view?.showErrorBar("Downloaded body was empty!", true, R.string.clear)
            }
        }

        override fun onRequestFailure(t: Throwable) {
            view?.showProgress(false)
            Logger.e(t, "Error while loading page content")
            t.message?.let { view?.showErrorBar(it, false, -1) }
            view?.clearAddress()
        }

        override fun onResponseNotOk(code: Int, raw: Response) {
            view?.showProgress(false)
            manageError(code, raw)
        }
    }
}
