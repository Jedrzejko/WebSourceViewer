package com.verify.bovquier.websourceviewer.addressreader.api.datamodel

import com.verify.bovquier.websourceviewer.base.BaseDataModel

/**
 * @author bovquier
 * on 16.09.2017.
 */

data class WebContent(val content: String) : BaseDataModel
