package com.verify.bovquier.websourceviewer.addressreader.contract

import androidx.annotation.StringRes

import com.verify.bovquier.websourceviewer.addressreader.api.datamodel.WebContent
import com.verify.bovquier.websourceviewer.base.BaseContract

/**
 * @author bovquier
 * on 15.09.2017.
 */

interface AddressReaderContract {
    interface View : BaseContract.View {

        fun getWebAddress(): String

        fun showInputError(@StringRes errorMsgId: Int)

        fun showErrorBar(msg: String, hasAction: Boolean, @StringRes actionTextId: Int)

        fun showRetryBar(@StringRes msgId: Int)

        fun clearAddress()

        fun showProgress(visible: Boolean)

        fun showContent(content: String)
    }

    interface Presenter<V : BaseContract.View, M : BaseContract.Model<WebContent>> : BaseContract.Presenter<V, M> {
        fun submitWebAddress()
    }

    interface Model : BaseContract.Model<WebContent> {
        fun loadWebContent(webAddress: String)

        fun setWebContent(webAddress: String, body: WebContent)
    }

    interface Service : BaseContract.Service<WebContent> {
        fun setWebAddress(webAddress: String)
    }
}
