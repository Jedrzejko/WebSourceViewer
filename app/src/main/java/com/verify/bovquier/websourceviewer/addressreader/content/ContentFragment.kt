package com.verify.bovquier.websourceviewer.addressreader.content

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.verify.bovquier.websourceviewer.R
import kotlinx.android.synthetic.main.fragment_content.*

/**
 * @author bovquier
 * on 17.09.2017.
 */

class ContentFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_content, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tv_content.text = arguments?.getCharSequence(KEY_CONTENT)
    }

    companion object {
        private const val KEY_CONTENT = "KEY_CONTENT"

        fun getInstance(content: String): Fragment {
            val fragment = ContentFragment()
            val bundle = Bundle()
            bundle.putCharSequence(KEY_CONTENT, content)
            fragment.arguments = bundle
            return fragment
        }
    }
}

