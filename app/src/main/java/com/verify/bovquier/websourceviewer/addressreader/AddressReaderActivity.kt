package com.verify.bovquier.websourceviewer.addressreader

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.annotation.StringRes
import com.google.android.material.snackbar.Snackbar
import androidx.fragment.app.Fragment
import androidx.core.content.ContextCompat
import android.view.View.*
import com.verify.bovquier.websourceviewer.R
import com.verify.bovquier.websourceviewer.addressreader.content.ContentFragment
import com.verify.bovquier.websourceviewer.addressreader.contract.AddressReaderContract
import com.verify.bovquier.websourceviewer.app.WsvApplication
import com.verify.bovquier.websourceviewer.base.BaseActivity
import kotlinx.android.synthetic.main.activity_address_reader.*
import javax.inject.Inject

class AddressReaderActivity : BaseActivity(), AddressReaderContract.View {
    override val layoutId: Int = R.layout.activity_address_reader
    @Inject
    lateinit var presenter: AddressReaderContract.Presenter<AddressReaderContract.View, AddressReaderContract.Model>

    private val Activity.app: WsvApplication
        get() = application as WsvApplication

    private val component by lazy {
        app.component
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component.inject(this)
        acb_submit_web_address.setOnClickListener { requestPermissions() }
    }

    override fun attachViewToPresenter() {
        presenter.view = this
    }

    override fun showInputError(@StringRes errorMsgId: Int) {
        tiet_webb_address.error = getString(errorMsgId)
    }

    override fun showErrorBar(msg: String, hasAction: Boolean, @StringRes actionTextId: Int) {
        val snackbar = Snackbar.make(tiet_webb_address, msg, Snackbar.LENGTH_LONG)
        if (hasAction) {
            snackbar.setAction(actionTextId, SnackbarActions().clearListener)
        }
        snackbar.show()
    }

    override fun showRetryBar(msgId: Int) {
        val snackbar = Snackbar.make(tiet_webb_address, msgId, Snackbar.LENGTH_LONG)
        snackbar.setAction(R.string.retry, SnackbarActions().retryActionListener)
        snackbar.show()
    }

    override fun getWebAddress(): String = tiet_webb_address.text.toString()

    override fun clearAddress() {
        tiet_webb_address.text = null
        tiet_webb_address.error = null
    }

    override fun showProgress(visible: Boolean) {
        pb_progress.visibility = if (visible) VISIBLE else GONE
    }

    override fun showContent(content: String) {
        val fm = supportFragmentManager
        fm.beginTransaction()
                .replace(R.id.fl_source_container_frame, ContentFragment.getInstance(content),
                        TAG_CONTENT_FRAGMENT
                ).commit()
    }

    private fun requestPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkPermission(Manifest.permission.INTERNET) || checkPermission(Manifest.permission.ACCESS_NETWORK_STATE)) {
                requestPermissions(arrayOf(Manifest.permission.INTERNET, Manifest.permission.ACCESS_NETWORK_STATE),
                        PERMISSION_REQUEST
                )
            }
        }
        removeFragment()
        presenter.submitWebAddress()
    }

    private fun checkPermission(accessNetworkState: String): Boolean {
        return ContextCompat.checkSelfPermission(this, accessNetworkState) != PackageManager.PERMISSION_GRANTED
    }

    private fun removeFragment(): Fragment? {
        val fm = supportFragmentManager
        val fragment = fm.findFragmentByTag(TAG_CONTENT_FRAGMENT)
        if (fragment != null) fm.beginTransaction().remove(fragment).commit()
        return fragment
    }

    override fun onBackPressed() {
        val fragment = removeFragment()
        if (fragment == null) {
            super.onBackPressed()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_REQUEST) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                presenter.submitWebAddress()
            }
        }
    }

    private inner class SnackbarActions {
        internal val retryActionListener: OnClickListener = OnClickListener { presenter.reload() }

        internal val clearListener: OnClickListener = OnClickListener { presenter.clearAddress() }
    }

    companion object {

        private val PERMISSION_REQUEST = 111
        private val TAG_CONTENT_FRAGMENT = "TAG_CONTENT_FRAGMENT"
    }
}
