package com.verify.bovquier.websourceviewer.addressreader.repository

import com.orhanobut.hawk.Hawk
import com.verify.bovquier.websourceviewer.addressreader.api.datamodel.WebContent

/**
 * @author bovquier
 * on 17.09.2017.
 */

open class ContentRepository {

    fun hasContent(webAddress: String): Boolean = Hawk.contains(webAddress)

    fun getContent(webAddress: String): WebContent = Hawk.get(webAddress)

    fun saveWebSite(webAddress: String, body: WebContent) {
        Hawk.put(webAddress, body)
    }
}
