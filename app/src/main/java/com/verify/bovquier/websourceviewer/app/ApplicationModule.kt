package com.verify.bovquier.websourceviewer.app

import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * @author bovquier
 * on 16.09.2017.
 */

@Module
class ApplicationModule(private val mApplication: WsvApplication) {
    @Provides
    @Singleton
    fun provideApplication(): WsvApplication = mApplication
}
